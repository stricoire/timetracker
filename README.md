# TimeTracker


This OpenOffice spreadsheet will help you to keep track of time spent on your projects:
- Single user oriented to keep it simple,
- Easy to add projects and specific tasks,
- Shows a clear summary of your current and previous week, and how you spent your time globally,


## How to use it for you personal projects

### Structure of spreadsheet

- The spreadsheet is composed of 3 main components : <b> Task </b>, <b> Project </b> and <b> Overview </b>.
<br>

- <b> Task </b> : Is the spreadsheet where you'll find the specific task related to your projects (<i> for eg : Game dev, Design, Sound Design, Communication etc.</i>). <br>
Feel free to modify it, naming the task as you want, as it will automatically actualize in other spreadsheet !
> <i>⚠️ Keep in mind how many task you create, as in the following sheets, you'll have to have the same number of lines </i>

<br>

- <b> Project spreadsheet </b>: are the spreadsheets which will represent each one of your projects. <br>
    - It could be the name of the video game you're working on
(<i>here it's `MyProject1` and `MyProject2`</i>). <br>
    - Inside those spreadsheet, you'll be
able to specify each time you worked on a project, how many time you worked on it, when it
was and on what specific task you worked (<i>here you'll be able to choose among the one you specified previously in `Task` spreadsheet</i>). <br><br>
It should look like this : <br><br>
![title](ReadmeScreens/ProjectTask.png) <br><br>
    - For each project you defined, you'll get on the right of the spreadsheet, for a given task the numbers of hours you spend on it (<i>weekly or in total </i>) <br><br>
    It should look like this : <br><br>
    ![title](ReadmeScreens/ProjectTotal.png) <br><br>
> <i>⚠️ If you previously added some new tasks in `Task` spreadsheet, don't forget to add here the wanted tasks, following the same pattern as the others. </i>

<br>

- <b> Overview </b> : Is the spreadsheet where you'll find the total hours you spend on each specific task for all your projects (<i>you'll also get the total hours per week</i>).
> <i>⚠️ If you previously added some new tasks in `Task` spreadsheet, don't forget to add here the wanted tasks, following the same pattern as the others. </i> <br>
> <i>⚠️ Each time you add a new project spreadsheet, don't forget to change the formula of the sum of hours to have it correclty working (for eg : B7 = MyProject1.N7 + MyProject2.N7 because I only get two projects)


## Authors and acknowledgment
This project is heavily inspired by `deepnight` Time Tracker Project. <br>
You could have more info on this [here](https://deepnight.net/blog/tools/free-time-tracking-tool/). <br>

Personaly, I choose to make my own one, because I wanted to rely on OpenSource software (not google one) and it was a fun way to learn spreadsheet way of coding stuff.

## Project status
Work in progress
